
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import generics

from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import employee,record,employee_image
from .serializers import recordSerializer
from rest_framework import status
from datetime import datetime
import os
import cv2
import pickle
import face_recognition as fr
import requests,json
from recognize import recognize
import sys
import time as t
sys.dont_write_bytecode = True



class recordList(APIView):

	def post(self,request):	
		
		image= request.data['profile_image']
		date=datetime.now()

		
		data={"Date":date,"name":"unknown","employee_id":"unknown","profile_image":image}
		
		
		serializer = recordSerializer(data=data)		
		if serializer.is_valid():
		 	id=serializer.save()
		 	record_instance=record.objects.get(pk=str(id))		 	
		 	employee_id=recognize(record_instance.profile_image)		 	
		 	if employee_id == "face not found":
		 		record_instance.delete()
		 		return HttpResponse(str(employee_id))

	 		elif employee_id == "unknown" :
	 			return HttpResponse(str(employee_id))

		 	else :
		 				 		
		 		emp= employee.objects.get(employee_id=employee_id)		 		
		 		record_instance.name=emp.name
		 		record_instance.employee_id=emp.employee_id
		 		record_instance.save()
		 		employee_id=record_instance.employee_id

		return HttpResponse(str(employee_id))
		

def train(request):	
	train_start_time=t.time()
	print(train_start_time)
	if os.path.isfile('training_model'):
		os.remove("training_model")

	known_encoding=[]
	known_names=[]
	filenames={}
	
	path1=os.getcwd()+"/photos/known"
	count=0
	for root,sub_dir,files in os.walk(path1):		
		for file in files:
			filenames[file]=os.path.join(root,file)
			
	
	for key in filenames:		
		try:
			if count%100==0:
				print(count)
				print("time taken from starting ",t.time()-train_start_time)
			count=count+1
			print("Training is Going On...")
			img=fr.load_image_file(filenames[key])
			face_location=fr.face_locations(img)
			a=fr.face_encodings(img,known_face_locations=face_location)[0]
			known_encoding.append(a)
			#ext= filename.split('.')[-1]

			#known_names.append(str(key.split('-')[0]))
			known_names.append(str(key)[:-5])			

		except:
			pass	
	data = {"encodings": known_encoding, "names": known_names}
	#print(data)
	f=open("training_model","wb")
	f.write(pickle.dumps(data))
	f.close()
	print("Our Model is Trained ")
	print("total training time",t.time()-train_start_time)
	return HttpResponseRedirect("../")