
import face_recognition as fr
import pickle
import sys
import os  
sys.dont_write_bytecode = True

a=pickle.loads(open("training_model","rb").read())

def recognize(image_path):
	img=fr.load_image_file(image_path)	
	try:
		face_location=fr.face_locations(img,model='cnn')
		face_encodings = fr.face_encodings(img,known_face_locations=face_location)[0]



		similarity=list(fr.face_distance(a["encodings"], face_encodings))
		#print(similarity)
		best_match=min(similarity)
		#print(best_match)
		name="unknown"
		if best_match <= 0.6:
			#print("hello world")
			index1=similarity.index(best_match)
			#print(index1)
			name = a["names"][index1]
			name=name.split('-')[0]
		return name



		# matches = fr.compare_faces(a["encodings"], face_encodings)
		# print(matches)
		# name ="unknown"		
		# if True in matches:
		#     first_match_index = matches.index(True)
		#     name = a["names"][first_match_index]		   	
		#     name=name.split('-')[0]	   
		# return name
    	  
	except:
		message="face not found"
		return message
	

