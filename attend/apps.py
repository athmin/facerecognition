# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import sys
sys.dont_write_bytecode = True


class AttendConfig(AppConfig):
    name = 'attend'
