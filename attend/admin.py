# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import Group,User
from .models import employee,record,employee_image
#from django.urls import path
import sys
sys.dont_write_bytecode = True

import cv2
import os
import numpy as np
import imutils
import face_recognition as fr
from imutils.video import VideoStream
import pickle
from django.conf.urls import url
from django.http import HttpResponseRedirect

class employeeAdmin(admin.ModelAdmin):
	list_display=('employee_id','name','Date')
	list_filter=('Date',)
	search_fields=('name','employee_id','Date')
	train_template = "attend/change_list.html"
	
	
	def get_queryset(self,request):
		queryset=super(employeeAdmin,self).get_queryset(request)
		queryset=queryset.order_by('employee_id')
		return queryset

class employee_image_Admin(admin.ModelAdmin):
	list_display=('sr_id','employee_id','Date','profile_image')
	list_filter=('Date','employee_id',)
	search_fields=('employee_id','Date')
	train_template = "attend/change_list.html"
	
	
	# def get_queryset(self,request):
	# 	queryset=super(employee_image_Admin,self).get_queryset(request)
	# 	queryset=queryset.order_by('sr_id')
	# 	return queryset	

class recordAdmin(admin.ModelAdmin):
	list_display=('sr_id','employee_id','name','Date','profile_image')
	search_fields=('sr_id','employee_id','Date')
	list_filter=('Date','employee_id')	


admin.site.site_header = "Face Recognition Based Attendance System"
admin.site.site_title = " Welcome Attendance System "
admin.site.index_title="Attendance system"
admin.site.register(employee,employeeAdmin)
admin.site.register(employee_image,employee_image_Admin)
admin.site.register(record,recordAdmin)

#admin.site.unregister(Group)
#admin.site.unregister(User)


