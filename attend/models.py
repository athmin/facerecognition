from __future__ import unicode_literals

from django.db import models
from datetime import datetime
import sys
import os
import uuid

sys.dont_write_bytecode = True
# Create your models here.




def rename_file(instance,filename):
	
	upload_to='photos/known/'
	ext= filename.split('.')[-1]
	
	print(instance)
	updated_name=str(instance) +str("-")+str(uuid.uuid4())[:8]	
	filename1 = '{}.{}'.format(updated_name, ext)	
	return os.path.join(upload_to, filename1)


class employee(models.Model):	
	employee_id = models.CharField(max_length=30,unique=True)
	name = models.CharField(max_length=30)
	Date= models.DateTimeField()

	def __str__(self):
	    return str(self.employee_id)


class employee_image(models.Model):	
	sr_id=models.AutoField(primary_key=True)
	employee_id = models.CharField(max_length=30)	
	profile_image = models.ImageField(upload_to=rename_file,blank=True, null=True,max_length=255)
	Date= models.DateTimeField()
	def __str__(self):
	    return str(self.employee_id)    
   	
   	

	
	
class record(models.Model):	
    sr_id=models.AutoField(primary_key=True)
    employee_id = models.CharField(max_length=80, default="unknown")
    name = models.CharField(max_length=30,default="unknown")
    profile_image = models.ImageField(upload_to='photos/unknown/',blank=True, null=True)
    Date=models.DateTimeField()

    def __str__(self):
        return str(self.sr_id)





    