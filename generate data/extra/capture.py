import cv2
import face_recognition as fr
import sys
sys.dont_write_bytecode = True
import recognize as r


def capture():
	video_capture = cv2.VideoCapture(0)
	count=0
	combined=""
	check=""
	message="Welcome to Athmin"
	while True:
		#print(count)
		#print("############")
		#print(combined)
		ret, frame = video_capture.read()
		face_locations = fr.face_locations(frame)
		if (len(face_locations)>=1):		
			cv2.imwrite("photos/"+"unknown"+"/"+"cap"+"%d.jpg" %count,frame)

			#cv2.imshow('face_recognition',frame)
			#cv2.waitKey(600)
			message=r.recognize("photos/"+"unknown"+"/"+"cap"+"%d.jpg" %count)
			print(message)
			combined=combined+str(message)
			print(combined)
			check=message*5
			if check in combined:
				r.text_to_speech(message)
				combined=combined.replace(message*5,'')
			count+=1
			#key = cv2.waitKey(1) & 0xFF
			#if key == ord("q"):
			#	break
	video_capture.release()
	cv2.destroyAllWindows()	

capture()

