import cv2
import os
import numpy as np
import imutils
import face_recognition as fr
from imutils.video import VideoStream
import pickle
import sys
sys.dont_write_bytecode = True

def add():
	
	user_name=input("Enter the name of user :")	

	video_capture = cv2.VideoCapture(0)

	count=0

	while True:
		ret, frame = video_capture.read()
		face_locations = fr.face_locations(frame)		
		cv2.imshow("Frame", frame)
		key = cv2.waitKey(1) & 0xFF

		if key == ord("k"):
			#if (len(face_locations)>=0):
				cv2.imwrite("photos/"+"known"+"/"+user_name+"%d.jpg" % count,frame)
				count+=1
		elif key == ord("q"):
			break
	print("Total No of images Stored: {}".format(count))
	video_capture.release()
	cv2.destroyAllWindows()


add()