from django.conf.urls import url
from django.contrib import admin
from attend import views
from django.conf import settings
from django.conf.urls.static import static
import sys
sys.dont_write_bytecode = True


urlpatterns = [
    url(r'^', admin.site.urls), 
    url(r'^attend/employee/train', views.train), 
    url(r'^attend/employee_image/train', views.train), 
    url(r'^record/', views.recordList.as_view()), 
       
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

