
import face_recognition as fr
import pickle
import sys
import os  
import cv2

a=pickle.loads(open("training_model", "rb").read())
def recognize():
	
	video_capture = cv2.VideoCapture(0)


	face_locations = []
	face_encodings = []
	face_names = []
	process_this_frame = True

	while True:
	    
	    ret, frame = video_capture.read()

	    
	    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

	    rgb_small_frame = small_frame[:, :, ::-1]

	    
	    if process_this_frame:
	        
	        face_locations = fr.face_locations(rgb_small_frame)
	        face_encodings = fr.face_encodings(rgb_small_frame, face_locations)

	        for face_encoding in face_encodings:
	            
	            matches = fr.compare_faces(a["encodings"], face_encoding)
	            name = "Unknown"

	            
	            if True in matches:
	                first_match_index = matches.index(True)
	                name = a["names"][first_match_index][:-1]

	            face_names.append(name)

	    process_this_frame = not process_this_frame


	    
	    for (top, right, bottom, left), name in zip(face_locations, face_names):
	        
	        top *= 4
	        right *= 4
	        bottom *= 4
	        left *= 4

	        
	        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

	        
	        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
	        font = cv2.FONT_HERSHEY_DUPLEX
	        cv2.putText(frame, name[:11], (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
	        

	   
	    cv2.imshow('Video', frame)

	    
	    if cv2.waitKey(1) & 0xFF == ord('q'):
	        break

		

	video_capture.release()
	cv2.destroyAllWindows()

recognize()
